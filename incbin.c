#ifdef _MSC_VER
#  define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>

#ifndef PATH_MAX
#  define PATH_MAX 260
#endif

#define SEARCH_PATHS_MAX 64
#define FILE_PATHS_MAX 1024

static int fline(char **line, size_t *n, FILE *fp) {
    int chr;
    char *pos;
    if (!line || !n || !fp)
        return -1;
    if (!*line)
        if (!(*line = (char *)malloc((*n=64))))
            return -1;
    chr = *n;
    pos = *line;
    for (;;) {
        int c = fgetc(fp);
        if (chr < 2) {
            *n += (*n > 16) ? *n : 64;
            chr = *n + *line - pos;
            if (!(*line = (char *)realloc(*line,*n)))
                return -1;
            pos = *n - chr + *line;
        }
        if (ferror(fp))
            return -1;
        if (c == EOF) {
            if (pos == *line)
                return -1;
            else
                break;
        }
        *pos++ = c;
        chr--;
        if (c == '\n')
            break;
    }
    *pos = '\0';
    return pos - *line;
}

static FILE *open_file(const char *name, const char *mode, const char (*searches)[PATH_MAX], int count) {
    int i;
    for (i = 0; i < count; i++) {
        char buffer[FILENAME_MAX + PATH_MAX];
        FILE *fp;
#ifndef _MSC_VER
        snprintf(buffer, sizeof(buffer), "%s/%s", searches[i], name);
#else
        _snprintf(buffer, sizeof(buffer), "%s/%s", searches[i], name);
#endif
        if ((fp = fopen(buffer, mode)))
            return fp;
    }
    return !count ? fopen(name, mode) : NULL;
}

static int strcicmp(const char *s1, const char *s2) {
    const unsigned char *us1 = (const unsigned char *)s1,
                        *us2 = (const unsigned char *)s2;
    while (tolower(*us1) == tolower(*us2)) {
        if (*us1++ == '\0')
            return 0;
        us2++;
    }
    return tolower(*us1) - tolower(*us2);
}

/* styles */
enum { kCamel, kSnake };
/* identifiers */
enum { kData, kEnd, kSize };

static const char *styled(int style, int ident) {
    switch (style) {
    case kCamel:
        switch (ident) {
        case kData: return "Data";
        case kEnd: return "End";
        case kSize: return "Size";
        }
        break;
    case kSnake:
        switch (ident) {
        case kData: return "_data";
        case kEnd: return "_end";
        case kSize: return "_size";
        }
        break;
    }
    return "";
}